import * as THREE from './three.module.js';
import { OrbitControls } from './OrbitControls.js';
import { GLTFLoader } from './GLTFLoader.js';
import { RGBELoader } from './RGBELoader.js';

let camera, scene, renderer;

init();
render();

//dom elements 
let liElements;

function init() {

    const loader = new GLTFLoader();

    const container = document.createElement('div');
    document.body.appendChild(container);

    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.10000000149011612, 1000);

    camera.position.set(0.0034498125314712524, 1.4816535711288452, 6.352334499359131);

    camera.fov = 22.89519204617112;
    camera.focus = 10;
    camera.aspect = 1.7777777777777777;
    camera.updateProjectionMatrix();
    scene = new THREE.Scene();


    const material = new THREE.MeshPhongMaterial({
        color: 0x0095DD
    });

    const light = new THREE.AmbientLight(0x544E4D); // soft light
    light.intensity = 1;
    scene.add(light);

    let modelOne;

    // model
    scene.background = new THREE.Color('#544E4D');

    function matchBlenderIntencity(val) {
        return val / 10;
    }

    loader.load('models/lights_4.gltf', function (gltf) {

        gltf.scene.children[0].children[0].intensity = matchBlenderIntencity(gltf.scene.children[0].children[0].intensity);
        gltf.scene.children[1].children[0].intensity = matchBlenderIntencity(gltf.scene.children[1].children[0].intensity);
        gltf.scene.children[2].children[0].intensity = matchBlenderIntencity(gltf.scene.children[2].children[0].intensity);
        gltf.scene.children[3].children[0].intensity = matchBlenderIntencity(gltf.scene.children[3].children[0].intensity);

        scene.add(gltf.scene);

        console.log(gltf.scene);

        render();

    });


    // loader.load('models/pupin_head.gltf', function (gltf) {

    //   scene.add(gltf.scene);

    //   render();

    // });

    loader.load('models/pupin_body.gltf', function (gltf) {

        scene.add(gltf.scene);

        render();

    });


    loader.load('models/table_carcas.gltf', function (gltf) {

        scene.add(gltf.scene);

        render();

    });


    loader.load('models/entire_scene.gltf', async function (gltf) {

        // gltf.scene.receiveShadow = true;
        // gltf.scene.receiveShadow = true;

        // gltf.scene.children[0].castShadow = true;
        // gltf.scene.children[0].receiveShadow = true;

        //gltf.scene.children.splice(90, 52);


        const donationsListWrapper = document.querySelector("#donationsList");
        const donationsList = document.querySelector("#donationsList ul");

        // try {
        //     const response = await fetch('http://localhost:8080/api?url=https://www.gofundme.com/charity/razom-razom-for-ukraine?modal=donations');
        //     const data = await response.json();
        //     console.log(data);

        //     for (let index = 0; index < data.length; index++) {
        //         const element = data[index];
        //         const li = `<li>
        //             <span>${element.name}</span>
        //             <span>${element.amount}</span>
        //         </li>`;

        //         donationsList.insertAdjacentHTML( 'beforeend', li );
        //     }

        // } catch (e) {
        //     console.log(e);
        // }


        liElements = document.querySelectorAll("#donationsList ul li");


        donationsListWrapper.addEventListener("scroll", donationsOnScroll);


        console.log(gltf.scene);

        scene.add(gltf.scene);

        render();

    });


    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.physicallyCorrectLights = true;
    container.appendChild(renderer.domElement);

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change', render); // use if there is no animation loop

    controls.minDistance = 5;
    controls.maxDistance = 8.5;

    controls.maxPolarAngle = Math.PI / 2;

    controls.target.set(0, 1, -2);

    controls.update();
    onWindowResize();

    window.addEventListener('resize', onWindowResize);


}


function onWindowResize() {

    let canvasHeight = window.innerHeight - 100;

    camera.aspect = window.innerWidth / canvasHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, canvasHeight);

    render();

}


function render() {

    renderer.render(scene, camera);

}

function donationsOnScroll(e) {

    //document.querySelectorAll("#donationsList ul li")[4].getBoundingClientRect().x

    // run every 10 scrolled pixels to avoid exploding a device of an unsuspecting visitor 
    if (e.target.scrollLeft % 10 == 0) {

        //console.log(e.target.querySelectorAll("li")[0].getBoundingClientRect().x);

        //let testFirst = (e.target.querySelectorAll("li")[0].getBoundingClientRect().x - (window.innerWidth / 2)) *-1;

        //console.log(e.target.scrollLeft == testFirst);
        //console.log(e.target.scrollLeft);

        //console.log(e.target.querySelectorAll("li")[0].getBoundingClientRect().x == (window.innerWidth / 2));

        //console.log(testFirst);

        for (let index = 0; index < liElements.length; index++) {
            const element = liElements[index];
            const activeArea = {
                start: element.getBoundingClientRect().x,
                end: element.getBoundingClientRect().x + element.getBoundingClientRect().width
            };
            if ((window.innerWidth / 2) >= activeArea.start && (window.innerWidth / 2) <= activeArea.end) {
                element.classList.add("current");
            } else {
                element.classList.remove("current");
            }
        }

    }
}